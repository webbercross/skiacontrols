# SkiaControls
## A light-weight demo framework to show how to create interactive 2D drawings on .NET MAUI

The demo was created from an existing project to allow forms style controls to be accurately added to complex schematic drawings.

Rather than overlaying xaml controls over a drawing and having difficulty with layout, these controls can be precisely placed anywhere on a drawing using 2D coordinates.

In the example drawing a grid sizing strategy is used to help create a drawing to fit the provided area without using scaling which can cause rendering issues.

The app uses an MVVM pattern using Prism Library.

## History
The initial demo was written in Xamarin.Forms using SkiaSharp for drawing. Since then, the app has been migrated to .NET Maui and more recently SkiaSharp replaced with Maui.Graphics. The Maui.Graphics api is very similar to SkiaSharp; however SkiaSharp offers more advanced effects. 

There was originally an 'Icon' control which used a matrix colour transform to produce and icon of any colour from a png icon. This is not possible in Maui.Graphics

## The Code
### DrawingBase
This class provides the main framework for interacting with the drawing canvas.
IDrawableControl items are created and registered with resources such as paints cached.

The drawing base allows touch events to be intercepted and passed the correct item in the drawing for handling.

### DrawableControl
This is a basic IDrawableControl item which holds no state and just renders a static 2d item

### ControlBase
This is a more advanced IDrawable item which holds state and can render differently depending what state it's in.

### Button
This is a simple button control which has Default, Enabled and Active states with different appearances for each. A Tapped event is fired when the button detects a tap release which the drawing can respond to.

### Label
The label has a configurable appearance and text which can be set.

## View model interaction
When view model owns the drawing in this example so in theory it could expose controls which could be set, however the IDrawing interface offers a RequestValue event for the drawing to ask for something and an OnValue callback for the view model to return the requested value.

## The demo
The demo is very basic. I just added some simple 2D shapes with some example of incrementing a counter, setting time in a label, randomly changing colours and requesting text from the view model.

