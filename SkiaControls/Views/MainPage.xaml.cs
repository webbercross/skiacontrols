﻿using System.ComponentModel;
using SkiaControls.ViewModels;

namespace SkiaControls
{
    [DesignTimeVisible(true)]
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (propertyName == nameof(BindingContext) && BindingContext is MainPageViewModel vm)
            {
                vm.SetGraphicsView(GraphicsView);
            }
        }
    }
}
