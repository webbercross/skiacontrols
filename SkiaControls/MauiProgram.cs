﻿using CommunityToolkit.Maui;
using SkiaControls.ViewModels;

namespace SkiaControls
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            return MauiApp.CreateBuilder()
                .UseMauiApp<App>()
                .UseMauiCommunityToolkit()
                .UsePrism(prism =>
                prism
                    .RegisterTypes(containerRegistry =>
                    {
                        //containerRegistry.RegisterForNavigation<NavigationPage>();
                        containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
                    })
                    .CreateWindow(async navigationService =>
                    {
                        var result = await navigationService.NavigateAsync("/NavigationPage/MainPage");
                        if (!result.Success)
                        {
                            System.Diagnostics.Debugger.Break();
                        }
                    })
                    )
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                })
            .Build();
        }
    }
}

