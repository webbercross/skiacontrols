﻿using System.Windows.Input;
using SkiaControls.Drawing;

namespace SkiaControls.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private  bool _isValueEntryVisible;
        private  string _value;
        private  RequestValueArgs _currentValueRequest;
        private IDrawing _drawing;
    
        public ICommand StartTouchCommand { get; }
        public ICommand EndTouchCommand { get; }
        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }
        public ICommand ResetCommand { get; }
        
        public bool IsValueEntryVisible
        {
            get => _isValueEntryVisible;
            set
            {
                if (value == _isValueEntryVisible)
                    return;

                _isValueEntryVisible = value;
                RaisePropertyChanged(nameof(IsValueEntryVisible));
            }
        }
        
        public string Value
        {
            get => _value;
            set
            {
                if (value == _value)
                    return;

                _value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }
    
        public MainPageViewModel(INavigationService navigationService)
            :base(navigationService)
        {
            Title = "SkiaControls Demo";
            
            _drawing = new DemoDrawing();
            _drawing.RequestValue += Drawing_RequestValue;
        
            StartTouchCommand = new DelegateCommand<TouchEventArgs>(e => OnTouch(e, TouchAction.Pressed));
            EndTouchCommand = new DelegateCommand<TouchEventArgs>(e => OnTouch(e, TouchAction.Released));
            OkCommand = new DelegateCommand(OnOk);
            CancelCommand = new DelegateCommand(() => IsValueEntryVisible = false);
            ResetCommand = new DelegateCommand(OnReset);            
        }

        public void SetGraphicsView(GraphicsView graphicsView)
        {
            graphicsView.Drawable = _drawing;
            _drawing.CanvasView = graphicsView;
            
            graphicsView.Invalidate();
        }

        void Drawing_RequestValue(object sender, RequestValueArgs e)
        {
            _currentValueRequest = e;
            
            IsValueEntryVisible = true;
        }

        void OnTouch(TouchEventArgs args, TouchAction action)
        {
            _drawing.OnTouch(args, action);
        }
        
        void OnReset()
        {
            _drawing.Reset();
        }
        
        void OnOk()
        {
            _drawing.OnValue(new ValueResult
            {
                Request = _currentValueRequest,
                Value = _value
            });            
        
            IsValueEntryVisible = false;
        }
    }
}
