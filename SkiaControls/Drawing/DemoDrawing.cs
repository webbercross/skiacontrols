﻿
namespace SkiaControls.Drawing
{
    public class DemoDrawing : DrawingBase
    {        
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        private Label _label1;
        private Label _label2;
        private Label _label3;
        private Label _label4;
        private Label _label5;

        private readonly SolidPaint _spotPaint = new(Colors.RoyalBlue);

        private int _counter;
        private int _seed;

        protected override void CreateDrawables(ICanvas canvas, RectF rect)
        {
            var gridSize = rect.Width / 32;

            // Draw some rectangle borders
            CreateFillRectangle(Colors.DodgerBlue, new Rect(gridSize * 1.0f, gridSize * 1.0f, gridSize * 30f, gridSize * 26f));
            CreateGradientRectangle(Colors.Black, Colors.DodgerBlue, new Rect(gridSize * 2f, gridSize * 2f, gridSize * 28f, gridSize * 24f));
            CreateBorderRectangle(Colors.RoyalBlue, new Rect(gridSize * 2f, gridSize * 2f, gridSize * 28f, gridSize * 24f), 5);
            
            // Add some static ovals
            // Basic shapes are added as drawables and do not hold state
            CreateFillOvalCentred(Colors.Red, gridSize * 16f, gridSize *  6f, gridSize * 3, gridSize * 3);
            CreateFillOvalCentred(Colors.Orange, gridSize * 16f, gridSize * 10f, gridSize * 4f, gridSize * 4f);
            CreateFillOvalCentred(Colors.Yellow, gridSize * 16f, gridSize * 14f, gridSize * 3f, gridSize * 3f);
            CreateFillOvalCentred(Colors.LimeGreen, gridSize * 16f, gridSize * 18f, gridSize * 2f, gridSize * 2f);
            CreateFillOvalCentred(Colors.Green, gridSize * 16f, gridSize * 22f, gridSize * 1f, gridSize * 1f);
            
            // Some spots round the borders            
            var spotSpacing = gridSize * 1f;
            var spotWidth = spotSpacing * 0.6f;
            
            var spotX = gridSize * 3f + gridSize * 0.2f;
            var spotY = gridSize * 3f + gridSize * 0.2f;
            while (spotX <= gridSize * 28f + gridSize * 0.2f)
            {
                CreateFillOval(_spotPaint, spotX, spotY, spotWidth, spotWidth);
                spotX += spotSpacing;
            }
            
            spotX = gridSize * 3f + gridSize * 0.2f;
            spotY = gridSize * 24f + gridSize * 0.2f;
            while (spotX <= gridSize * 28f + gridSize * 0.2f)
            {
                CreateFillOval(_spotPaint, spotX, spotY, spotWidth, spotWidth);
                spotX += spotSpacing;
            }
            
            spotX = gridSize * 3f + gridSize * 0.2f;
            spotY = gridSize * 3f + gridSize * 0.2f;
            while (spotY <= gridSize * 24f + gridSize * 0.2f)
            {
                CreateFillOval(_spotPaint, spotX, spotY, spotWidth, spotWidth);
                spotY += spotSpacing;
            }
            
            spotX = gridSize * 28f + gridSize * 0.2f;
            spotY = gridSize * 3f + gridSize * 0.2f;
            while (spotY <= gridSize * 24f + gridSize * 0.2f)
            {
                CreateFillOval(_spotPaint, spotX, spotY, spotWidth, spotWidth);
                spotY += spotSpacing;
            }

            // Some buttons down the left side
            // Controls like buttons hold state and have their own draw methods
            _button1 = new Button(this)
            {
                Id = "button1",
                LayoutBounds = new Rect(gridSize * 4, gridSize * 4, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Increment",
                TextSize = gridSize
            };
            _button1.Tapped += (_, _) => _label1.Text = (++_counter).ToString();
            AddDrawable(_button1);

            _button2 = new Button(this)
            {
                Id = "button2",
                LayoutBounds = new Rect(gridSize * 4, gridSize * 8, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                ActiveColor = Colors.Lime,
                TextColor = Colors.Blue,
                BorderColor = Colors.Red,
                Text = "Time",
                TextSize = gridSize
            
            };
            _button2.Tapped += (_, _) => _label2.Text = DateTime.Now.ToLongTimeString();
            AddDrawable(_button2);
            
            _button3 = new Button(this)
            {
                Id = "button3",
                LayoutBounds = new Rect(gridSize * 4, gridSize * 12, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Disabled",
                TextSize = gridSize,
                IsEnabled = false
            };
            AddDrawable(_button3);
            
            _button4 = new Button(this)
            {
                Id = "button4",
                LayoutBounds = new Rect(gridSize * 4, gridSize * 16, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Rnd Color",
                TextSize = gridSize,
                BorderWidth = 6
            };
            _button4.Tapped += (_, _) =>
            {
                _button4.RedrawEnabled = false;
                _label4.RedrawEnabled = false;
            
                _button4.DefaultColor = GetRandomColor();
                _button4.TextColor = GetRandomColor();
                _button4.BorderColor = GetRandomColor();
                _button4.ActiveColor = GetRandomColor();
            
                _label4.BackgroundColor = GetRandomColor();
                _label4.TextColor = GetRandomColor();
                _label4.BorderColor = GetRandomColor();
            
                _button4.RedrawEnabled = true;
                _label4.RedrawEnabled = true;
                
                Redraw();
            };
            AddDrawable(_button4);
            
            _button5 = new Button(this)
            {
                Id = "button5",
                LayoutBounds = new Rect(gridSize * 4, gridSize * 20, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Request",
                TextSize = gridSize
            };
            _button5.Tapped += (_, _) => OnRequestValue(new RequestValueArgs(_button5.Id, _label5.Id));
            AddDrawable(_button5);

            // Add some labels
            _label1 = new Label(this)
            {
                Id = "label1",
                LayoutBounds = new Rect(gridSize * 18, gridSize * 4, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = _counter.ToString(),
                TextSize = gridSize
            };
            AddDrawable(_label1);

            _label2 = new Label(this)
            {
                Id = "label2",
                LayoutBounds = new Rect(gridSize * 18, gridSize * 8, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Label 2",
                TextSize = gridSize
            };
            AddDrawable(_label2);
            
            _label3 = new Label(this)
            {
                Id = "label3",
                LayoutBounds = new Rect(gridSize * 18, gridSize * 12, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Label 3",
                TextSize = gridSize
            };
            AddDrawable(_label3);
            
            _label4 = new Label(this)
            {
                Id = "label4",
                LayoutBounds = new Rect(gridSize * 18, gridSize * 16, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Label 4",
                TextSize = gridSize,
                BorderWidth = 6
            };
            AddDrawable(_label4);
            
            _label5 = new Label(this)
            {
                Id = "label5",
                LayoutBounds = new Rect(gridSize * 18, gridSize * 20, gridSize * 10, gridSize * 4),
                Margin = new Thickness(5),
                Text = "Label 5",
                TextSize = gridSize
            };
            AddDrawable(_label5);
        }

        public override void OnValue(ValueResult result)
        {
            if (result.Request.DestinationId == _label5.Id)
                _label5.Text = (string)result.Value;
        }
        
        private Color GetRandomColor()
        {
            var rnd = new Random(_seed++);

            var red = (byte)rnd.Next(byte.MinValue, byte.MaxValue);
            var green = (byte)rnd.Next(byte.MinValue, byte.MaxValue);
            var blue = (byte)rnd.Next(byte.MinValue, byte.MaxValue);

            return new Color(red, green, blue);
        }
    }
}

