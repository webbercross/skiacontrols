﻿namespace SkiaControls.Drawing
{
    public interface IDrawableControl
    {
        void Draw(ICanvas canvas);
        bool IsVisible { get; set; }
    }
}
