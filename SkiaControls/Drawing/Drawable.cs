﻿
namespace SkiaControls.Drawing
{
    public class DrawableControl : IDrawableControl
    {
        public Action<ICanvas> DrawAction { get; set; }
        public bool IsVisible { get; set; } = true;
    
        public void Draw(ICanvas canvas)
        {
            DrawAction(canvas);
        }
    }
}
