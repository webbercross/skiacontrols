﻿namespace SkiaControls.Drawing
{
    public class Label : ControlBase, IDrawableControl
    {
        private Color _borderColor = Colors.DarkGray;
        private Color _textColor = Colors.Black;
        private Color _backgroundColor = Colors.LightGray;
        
        private Paint _backgroundPaint;        

        private float _borderWidth = 3;        
        private float _textSize = 10;
        private string _text;
        
        public string Text 
        { 
            get => _text; 
            set
            {
                if (_text == value)
                    return;
                    
                _text = value;
                Redraw();
            } 
        }
        public Rect Rect { get; set; }
        
        public Color BorderColor
        {
            get => _borderColor;
            set
            {
                if (Equals(_borderColor, value))
                    return;

                _borderColor = value;

                Redraw();
            }
        }
        
        public float BorderWidth
        {
            get => _borderWidth;
            set
            {
                if (_borderWidth == value)
                    return;

                _borderWidth = value;

                Redraw();
            }
        }
        
        public Color TextColor
        {
            get => _textColor;
            set
            {
                if (Equals(_textColor, value))
                    return;

                _textColor = value;

                Redraw();
            }
        }
        
        public Color BackgroundColor
        {
            get => _backgroundColor;
            set
            {
                if (Equals(_backgroundColor, value))
                    return;

                _backgroundColor = value;

                CreateBackgroundPaint();
                Redraw();
            }
        }
        
        public float TextSize
        {
            get => _textSize;
            set
            {
                if (_textSize == value)
                    return;

                _textSize = value;

                Redraw();
            }
        }
        
        public Label(DrawingBase parent)
            : base(parent)        
        {
            CreateBackgroundPaint();
        }
        
        private void CreateBackgroundPaint()
        {
            _backgroundPaint = new SolidPaint(BackgroundColor);
        }
            
        public override void Draw(ICanvas canvas)
        {
            var drawingBounds = GetDrawingBounds();
            
            canvas.SetFillPaint(_backgroundPaint, drawingBounds);
            canvas.FillRectangle(drawingBounds);

            canvas.StrokeColor = _borderColor;
            canvas.StrokeSize = _borderWidth;
            canvas.DrawRectangle(drawingBounds);

            // Text  
            canvas.FontColor = _textColor;
            canvas.FontSize = _textSize;
            canvas.DrawString(Text, drawingBounds.X, drawingBounds.Y, drawingBounds.Width, drawingBounds.Height,
                HorizontalAlignment.Center, VerticalAlignment.Center);
        }
    }
}
