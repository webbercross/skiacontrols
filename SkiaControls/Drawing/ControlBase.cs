﻿
namespace SkiaControls.Drawing
{
    public enum TouchAction
    {
        Pressed,
        Released,
    }
    
    public abstract class ControlBase : IDrawableControl
    {
        public string Id { get; set; }
        public RectF LayoutBounds {get;set; }
        public bool IsEnabled { get; set; } = true;
        public Thickness Margin { get; set; }
        
        private readonly DrawingBase _parent;

        public bool RedrawEnabled { get; set; } = true;
        
        protected ControlBase(DrawingBase parent)
        {
            _parent = parent;
        }
        
        protected RectF GetDrawingBounds()
        {
            return new RectF(
                LayoutBounds.Left + (float)Margin.Left, 
                LayoutBounds.Top + (float)Margin.Top, 
                LayoutBounds.Width - (float)Margin.Right - (float)Margin.Left, 
                LayoutBounds.Height - (float)Margin.Bottom - (float)Margin.Top);
        }
        
        public bool HitTest(PointF hitPoint, TouchAction action)
        {
            // Check in both directions
            var inX = (hitPoint.X <= LayoutBounds.Right && hitPoint.X >= LayoutBounds.Left) || (hitPoint.X <= LayoutBounds.Left && hitPoint.X >= LayoutBounds.Right);
            var inY = (hitPoint.Y <= LayoutBounds.Top && hitPoint.Y >= LayoutBounds.Bottom) || (hitPoint.Y <= LayoutBounds.Bottom && hitPoint.Y >= LayoutBounds.Top);
        
            var hit = inX && inY;
            
            if(hit)
            {
                OnHit(action);
            }
        
            return hit;
        }
        
        protected virtual void OnHit(TouchAction action)
        {
        
        }
        
        protected void Redraw()
        {
            if (RedrawEnabled)
                _parent.Redraw();
        }

        public abstract void Draw(ICanvas canvas);

        public bool IsVisible { get; set; } = true;
    }
}
