﻿
namespace SkiaControls.Drawing
{
    public abstract class DrawingBase : IDrawing
    {
        private int _id;
        private readonly Dictionary<string, IDrawableControl> _drawableDictionary = new Dictionary<string, IDrawableControl>();

        private IEnumerable<IDrawableControl> Drawables => _drawableDictionary.Values;

        public IGraphicsView CanvasView { get; set; }
        public event EventHandler<RequestValueArgs> RequestValue;
        
        protected void AddDrawable(IDrawableControl drawable)
        {
            AddDrawable(_id++, drawable);
        }

        private void AddDrawable(int id, IDrawableControl drawable)
        {
            AddDrawable(id.ToString(), drawable);
        }

        private void AddDrawable(string id, IDrawableControl drawable)
        {
            _drawableDictionary.Add(id, drawable);
        }

        protected void AddDrawable(Action<ICanvas> action)
        {
            AddDrawable(_id++, new DrawableControl
            {
                DrawAction = action
            });
        }

        protected T Get<T>(string id) where T : IDrawableControl
        {
            if(!_drawableDictionary.ContainsKey(id))
            {
                throw new Exception("Drawable not registered");
            }

            var d = _drawableDictionary[id];
            
            if(d.GetType() != typeof(T))
            {
                throw new Exception("Drawable is not correct type");
            }

            return (T)d;
        }
        
        private IEnumerable<T> GetAll<T>() where T : IDrawableControl
        {
            var t = typeof(T);
            
            return Drawables
                .Where(d => d.GetType() == t || d.GetType().BaseType == t)
                    .Select(d => (T)d);
        }
        
        protected abstract void CreateDrawables(ICanvas canvas, RectF rect);
        
        public virtual void OnTouch(TouchEventArgs args, TouchAction touchAction)
        {
            if (!args.IsInsideBounds)
            {
                return;
            }

            if (args.Touches == null || args.Touches.Length == 0)
            {
                return;
            }

            var controls = GetAll<ControlBase>()
                .Where(c => c.IsEnabled && c.IsVisible);

            var needsInvalidate = controls.Any(c => c.HitTest(args.Touches.Last(), touchAction));

            if (needsInvalidate)
            { 
                CanvasView.Invalidate();
            }         
        }              
        
        public virtual void OnValue(ValueResult result)
        {
        
        }
        
        public virtual void Reset()
        {

        }        
        
        protected void CreateButton(string id, string text, Rect rect, Action action)
        {
            AddDrawable(id, new Button(this)
            {
                Id = id,
                LayoutBounds = rect,
                Action = action,
                Text = text
            });
        }
        
        protected void CreateLabel(string id, string text, Rect rect)
        {
            AddDrawable(id, new Label(this)
            {
                Id = id,
                Text = text,
                Rect = rect
            });
        }
        
        internal void Redraw()
        {
            CanvasView?.Invalidate();
        }
        
        protected void OnRequestValue(RequestValueArgs args)
        {
            RequestValue?.Invoke(this, args);
        }

        public void Draw(ICanvas canvas, RectF rect)
        {
            if (!Drawables.Any())
                CreateDrawables(canvas, rect);
            
            foreach(var d in Drawables.Where(d => d.IsVisible))
            {
                d.Draw(canvas);
            }    
        }
        
        protected void CreateFillOval(Color color, float x, float y, float width, float height)
        {
            CreateFillOval(new SolidPaint(color), x, y, width, height);
        }
        
        protected void CreateFillOvalCentred(Color color, float centreX, float centreY, float width, float height)
        {
            var x = centreX - width / 2f;
            var y = centreY - height / 2f;
            
            CreateFillOval(new SolidPaint(color), x, y, width, height);
        }
        
        protected void CreateFillOval(SolidPaint paint, float x, float y, float width, float height)
        {
            AddDrawable(canvas =>
            {
                var rect = new Rect(x, y, width, height);
                canvas.SetFillPaint(paint, rect);
                canvas.FillEllipse(rect);
            });
        }
        
        protected void CreateFillRectangle(Color color, Rect rect)
        {
            AddDrawable((canvas) =>
            {
                var paint = new SolidPaint(color);
                canvas.SetFillPaint(paint, rect);
                canvas.FillRectangle(rect);
            });
        }
        
        protected void CreateGradientRectangle(Color color1, Color color2, Rect rect)
        {
            AddDrawable(canvas =>
            {
                var paint = new LinearGradientPaint
                {
                    StartColor = color1,
                    EndColor = color2,
                    StartPoint = new Point(0, 0),
                    EndPoint = new Point(1, 1)
                };
                canvas.SetFillPaint(paint, rect);
                canvas.FillRectangle(rect);
            });
        }
        
        protected void CreateBorderRectangle(Color color, Rect rect, float width)
        {
            AddDrawable(canvas =>
            {
                canvas.StrokeColor = color;
                canvas.StrokeSize = width;
                canvas.DrawRectangle(rect);
            });
        }
    }
}
