﻿
namespace SkiaControls.Drawing
{
    public class RequestValueArgs
    {
        public string SourceId { get; set; }
        public string DestinationId { get; set; }
        
        public RequestValueArgs(string sourceId, string destinationId)
        {
            SourceId = sourceId;
            DestinationId = destinationId;
        }
    }
    
    public class ValueResult
    {
        public RequestValueArgs Request { get; set; }
        public object Value { get; set; }
    }
    
    public interface IDrawing : IDrawable
    {
        event EventHandler<RequestValueArgs> RequestValue;
        void OnValue(ValueResult result);
        IGraphicsView CanvasView { get; set; }
        void OnTouch(TouchEventArgs args, TouchAction action);
        void Reset();
    }
}

