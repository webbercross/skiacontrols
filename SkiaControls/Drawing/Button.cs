﻿using Color = Microsoft.Maui.Graphics.Color;
using Paint = Microsoft.Maui.Graphics.Paint;
using RectF = Microsoft.Maui.Graphics.RectF;

namespace SkiaControls.Drawing
{
    public class Button : ControlBase, IDrawableControl
    {   
        private Color _defaultColor = Colors.LightGray;
        private Color _activeColor  = Colors.Red;
        private Color _disabledColor = Colors.Gray;
        private Color _borderColor = Colors.DarkGray;
        private Color _textColor = Colors.Black;
        
        private Paint _defaultPaint;
        private Paint _activePaint;
        private Paint _disabledPaint;

        private float _textSize = 10;
        private float _borderWidth = 3;
        private bool _active;

        public Action Action { get; init; }
        public string Text { get; init; }

        public event EventHandler Tapped;
        
        public Color DefaultColor
        {
            get => _defaultColor;
            set
            {
                if (Equals(_defaultColor, value))
                    return;

                _defaultColor = value;

                CreateDefaultPaint();
                Redraw();
            }
        }        
        
        public Color ActiveColor
        {
            get => _activeColor;
            set
            {
                if (Equals(_activeColor, value))
                    return;

                _activeColor = value;

                CreateActivePaint();
                Redraw();
            }
        }
        
        public Color DisabledColor
        {
            get => _disabledColor;
            set
            {
                if (Equals(_disabledColor, value))
                    return;

                _disabledColor = value;

                CreateDisabledPaint();
                Redraw();
            }
        }
        
        public Color BorderColor
        {
            get => _borderColor;
            set
            {
                if (Equals(_borderColor, value))
                    return;

                _borderColor = value;
                
                Redraw();
            }
        }
        
        public Color TextColor
        {
            get => _textColor;
            set
            {
                if (Equals(_textColor, value))
                    return;

                _textColor = value;

                Redraw();
            }
        }
        
        public float TextSize
        {
            get => _textSize;
            set
            {
                if (_textSize == value)
                    return;

                _textSize = value;

                //CreateTextPaint();
                Redraw();
            }
        }
        
        public float BorderWidth
        {
            get => _borderWidth;
            set
            {
                if (_borderWidth == value)
                    return;

                _borderWidth = value;

                //CreateBorderPaint();
                Redraw();
            }
        }

        public Button(DrawingBase parent)
            : base(parent)    
        {
            CreateDefaultPaint();
            CreateActivePaint();
            CreateDisabledPaint();
        }

        private void CreateDefaultPaint()
        {
            _defaultPaint = new SolidPaint(DefaultColor);
        }

        private void CreateActivePaint()
        {
            _activePaint = new SolidPaint(ActiveColor);
        }

        private void CreateDisabledPaint()
        {
            _disabledPaint = new SolidPaint(DisabledColor);
        }

        public override void Draw(ICanvas canvas)
        {
            var drawingBounds = GetDrawingBounds();

            if(!IsEnabled)
            {
                canvas.SetFillPaint(_disabledPaint, drawingBounds);
                canvas.FillRectangle(drawingBounds);                
            }
            else if (_active)
            {
                canvas.SetFillPaint(_activePaint, drawingBounds);
                canvas.FillRectangle(drawingBounds);        
            }
            else
            {
                canvas.SetFillPaint(_defaultPaint, drawingBounds);
                canvas.FillRectangle(drawingBounds);        
            }

            canvas.StrokeColor = _borderColor;
            canvas.StrokeSize = _borderWidth;
            canvas.DrawRectangle(drawingBounds);

            if (!string.IsNullOrWhiteSpace(Text))
                DrawText(canvas, drawingBounds);
            
        }
            
        private void DrawText(ICanvas canvas, RectF drawingBounds)
        {
            canvas.FontColor = _textColor;
            canvas.FontSize = _textSize;
            canvas.DrawString(Text, drawingBounds.X, drawingBounds.Y , drawingBounds.Width, drawingBounds.Height, HorizontalAlignment.Center, VerticalAlignment.Center);
        }

        protected override void OnHit(TouchAction action)
        {
            _active = action == TouchAction.Pressed;

            if (action != TouchAction.Released) return;
            
            Action?.Invoke();
            OnTapped();
        }
        
        private void OnTapped()
        {
            Tapped?.Invoke(this, EventArgs.Empty);
        }
    }
}
