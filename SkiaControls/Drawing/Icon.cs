﻿// using System.Reflection;
// using Microsoft.Maui.Graphics.Platform;
// using IImage = Microsoft.Maui.Graphics.IImage;
//
//
// namespace SkiaControls.Drawing
// {
//     public class Icon : Control, IDrawableControl
//     {
//         IImage _bitmap;
//         Color _color = Colors.LimeGreen;
//         Color _defaultColor = Colors.LightGray;
//     
//         Paint _iconPaint;
//         Paint _defaultPaint;
//     
//         string _fileName;
//     
//         public string Filename
//         {
//             get => _fileName;
//             set
//             {
//                 if (_fileName == value)
//                     return;
//     
//                 _fileName = value;
//                 CreateBitmap();
//                 Redraw();
//             }
//         }
//     
//         public Color DefaultColor
//         {
//             get => _defaultColor;
//             set
//             {
//                 if (_defaultColor == value)
//                     return;
//     
//                 _defaultColor = value;
//     
//                 CreateDefaultPaint();
//                 Redraw();
//             }
//         }
//     
//         public Color Color
//         {
//             get => _color;
//             set
//             {
//                 if (_color == value)
//                     return;
//     
//                 _color = value;
//     
//                 CreateIconPaint();
//                 Redraw();
//             }
//         }
//     
//         public Icon(DrawingBase parent)
//             : base(parent)    
//         {
//             CreateDefaultPaint();
//             CreateIconPaint();
//         }
//     
//         void CreateDefaultPaint()
//         {
//             _defaultPaint = new SolidPaint(DefaultColor);
//         }
//     
//         void CreateBitmap()
//         {
//             string resourceID = $"SkiaControls.Images.{Filename}";
//             var assembly = GetType().GetTypeInfo().Assembly;
//     
//             using (var stream = assembly.GetManifestResourceStream(resourceID))
//             {
//                 _bitmap = PlatformImage.FromStream(stream);
//             }
//         }
//     
//         void CreateIconPaint()
//         {
//             // https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/graphics/skiasharp/effects/color-filters
//     
//             // RGBA
//             var r = _color.Red;
//             var g = _color.Green;
//             var b = _color.Blue;
//             var a = _color.Alpha / 255f;
//     
//             a = 0.5f;
//     
//             _iconPaint = new ImagePaint
//             {
//                 // ColorFilter = ColorFilter.CreateColorMatrix(new float[]
//                 // {
//                 //     0, 0, 0, 0, r,
//                 //     0, 0, 0, 0, g,
//                 //     0, 0, 0, 0, b,
//                 //     0, 0, 0, a, 0,
//                 // })
//                 Image = _bitmap.Downsize(100),
//             };
//         }
//     
//         public override void Draw(ICanvas canvas)
//         {
//             var drawingBounds = GetDrawingBounds();
//     
//             canvas.SetFillPaint(_iconPaint, RectF.Zero);
//             canvas.FillRectangle(drawingBounds);
//         }
//     }
// }
